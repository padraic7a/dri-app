// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require video
//= require jquery
//= require jquery_ujs

//
// Required by Blacklight-Maps
//= require blacklight-maps
//= require jquery.remotipart
//= require jquery.cookie
//= require jquery.validate
//= require additional-methods
//= require bootstrap/affix
//= require bootstrap/carousel
//= require bootstrap/tooltip
//= require bootstrap/popover
//= require bootstrap/tab
//= require jquery.bootstrap-duallistbox
// Required by Blacklight
//= require blacklight/blacklight
//
//= require colorbox-rails
//= require social-share-button
//= require dri/

