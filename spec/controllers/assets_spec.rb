require 'spec_helper'

describe AssetsController do
  include Devise::TestHelpers

  before(:each) do
    @login_user = FactoryGirl.create(:admin)
    sign_in @login_user

    @collection = FactoryGirl.create(:collection)
   
    @object = FactoryGirl.create(:sound) 
    @object[:status] = "draft"
    @object.save

    @collection.governed_items << @object

    @collection.save    

    @tmp_upload_dir = Dir.mktmpdir
    @tmp_assets_dir = Dir.mktmpdir

    Settings.dri.uploads = @tmp_upload_dir
    Settings.dri.files = @tmp_assets_dir
  end

  after(:each) do
    @collection.delete
    @login_user.delete
    FileUtils.remove_dir(@tmp_upload_dir, :force => true)
    FileUtils.remove_dir(@tmp_assets_dir, :force => true)
  end

  describe 'create' do

    it 'should create an asset from a local file' do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)

      FileUtils.cp(File.join(fixture_path, "SAMPLEA.mp3"), File.join(@tmp_upload_dir, "SAMPLEA.mp3"))
      options = { :file_name => "SAMPLEA.mp3" }      
      post :create, { :object_id => @object.id, :local_file => "SAMPLEA.mp3", :file_name => "SAMPLEA.mp3" }
       
      expect(Dir.glob("#{@tmp_assets_dir}/**/SAMPLEA.mp3")).not_to be_empty
    end

    it 'should create an asset from an upload' do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)

      @uploaded = Rack::Test::UploadedFile.new(File.join(fixture_path, "SAMPLEA.mp3"), "audio/mp3")
      post :create, { :object_id => @object.id, :Filedata => @uploaded }

      expect(Dir.glob("#{@tmp_assets_dir}/**/SAMPLEA.mp3")).not_to be_empty
    end

    it 'should mint a doi when an asset is added to a published object' do
      @object.status = "published"
      @object.save

      DoiConfig = OpenStruct.new({ :username => "user", :password => "password", :prefix => '10.5072', :base_url => "http://repository.dri.ie", :publisher => "Digital Repository of Ireland" })
      Settings.doi.enable = true

      DataciteDoi.create(object_id: @object.id)

      DRI::Asset::Actor.any_instance.stub(:create_external_content).and_return(true)

      Sufia.queue.should_receive(:push).with(an_instance_of(MintDoiJob)).once
      @uploaded = Rack::Test::UploadedFile.new(File.join(fixture_path, "SAMPLEA.mp3"), "audio/mp3")
      post :create, { :object_id => @object.id, :Filedata => @uploaded }

      DataciteDoi.where(object_id: @object.id).first.delete
      DoiConfig = nil
      Settings.doi.enable = false
    end

   end

   describe 'update' do  
    it 'should create a new version' do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)
      DRI::Asset::Actor.any_instance.stub(:update_external_content)

      generic_file = DRI::GenericFile.new(id: ActiveFedora::Noid::Service.new.mint)
      generic_file.batch = @object
      generic_file.apply_depositor_metadata('test@test.com')
      file = LocalFile.new(fedora_id: generic_file.id, ds_id: "content")
      options = {}
      options[:mime_type] = "audio/mp3"
      options[:file_name] = "SAMPLEA.mp3"
       
      uploaded = Rack::Test::UploadedFile.new(File.join(fixture_path, "SAMPLEA.mp3"), "audio/mp3")
      file.add_file uploaded, options
      file.save
      generic_file.save
      file_id = generic_file.id

      @uploaded = Rack::Test::UploadedFile.new(File.join(fixture_path, "SAMPLEA.mp3"), "audio/mp3")
      put :update, { :object_id => @object.id, :id => file_id, :Filedata => @uploaded }
      expect(Dir.glob("#{@tmp_assets_dir}/**/content1/SAMPLEA.mp3")).not_to be_empty
    end

    it 'should create a new version from a local file' do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)
      DRI::Asset::Actor.any_instance.stub(:update_external_content)

      FileUtils.cp(File.join(fixture_path, "SAMPLEA.mp3"), File.join(@tmp_upload_dir, "SAMPLEA.mp3"))

      generic_file = DRI::GenericFile.new(id: ActiveFedora::Noid::Service.new.mint)
      generic_file.batch = @object
      generic_file.apply_depositor_metadata('test@test.com')
      file = LocalFile.new(fedora_id: generic_file.id, ds_id: "content")
      options = {}
      options[:mime_type] = "audio/mp3"
      options[:file_name] = "SAMPLEA.mp3"

      file.add_file File.new(File.join(@tmp_upload_dir, "SAMPLEA.mp3")), options
      file.save
      generic_file.save
      file_id = generic_file.id

      put :update, { :object_id => @object.id, :id => file_id, :local_file => "SAMPLEA.mp3", :file_name => "SAMPLEA.mp3" }
      expect(Dir.glob("#{@tmp_assets_dir}/**/content1/SAMPLEA.mp3")).not_to be_empty
    end

    it 'should mint a doi when an asset is modified' do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)
      DRI::Asset::Actor.any_instance.stub(:update_external_content).and_return(true)

      DoiConfig = OpenStruct.new({ :username => "user", :password => "password", :prefix => '10.5072', :base_url => "http://repository.dri.ie", :publisher => "Digital Repository of Ireland" })
      Settings.doi.enable = true

      FileUtils.cp(File.join(fixture_path, "SAMPLEA.mp3"), File.join(@tmp_upload_dir, "SAMPLEA.mp3"))

      generic_file = DRI::GenericFile.new(id: ActiveFedora::Noid::Service.new.mint)
      generic_file.batch = @object
      generic_file.apply_depositor_metadata('test@test.com')
      file = LocalFile.new(fedora_id: generic_file.id, ds_id: "content")
      options = {}
      options[:mime_type] = "audio/mp3"
      options[:file_name] = "SAMPLEA.mp3"

      file.add_file File.new(File.join(@tmp_upload_dir, "SAMPLEA.mp3")), options
      file.save
      generic_file.save
      file_id = generic_file.id

      @object.status = "published"
      @object.save
      DataciteDoi.create(object_id: @object.id)

      Sufia.queue.should_receive(:push).with(an_instance_of(MintDoiJob)).once
      put :update, { :object_id => @object.id, :id => file_id, :local_file => "SAMPLEA.mp3", :file_name => "SAMPLEA.mp3" }
       
      DataciteDoi.where(object_id: @object.id).each { |d| d.delete }
      DoiConfig = nil
      Settings.doi.enable = false
    end

  end

  describe 'destroy' do
    
    it 'should delete a file' do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)
      
      generic_file = DRI::GenericFile.new(id: ActiveFedora::Noid::Service.new.mint)
      generic_file.batch = @object
      generic_file.apply_depositor_metadata('test@test.com')
      file = LocalFile.new(fedora_id: generic_file.id, ds_id: "content")
      options = {}
      options[:mime_type] = "audio/mp3"
      options[:file_name] = "SAMPLEA.mp3"
       
      uploaded = Rack::Test::UploadedFile.new(File.join(fixture_path, "SAMPLEA.mp3"), "audio/mp3")
      file.add_file uploaded, options
      file.save
      generic_file.save
      file_id = generic_file.id

      expect {
        delete :destroy, object_id: @object.id, id: file_id
      }.to change { ActiveFedora::Base.exists?(file_id) }.from(true).to(false)
      
    end

  end

  describe 'download' do
  
    it "should be possible to download the master asset" do
      DRI::Asset::Actor.any_instance.stub(:create_external_content)

      @object.master_file_access = 'public'
      @object.edit_users_string = @login_user.email
      @object.read_users_string = @login_user.email
      @object.save
      @object.reload

      generic_file = DRI::GenericFile.new(id: ActiveFedora::Noid::Service.new.mint)
      generic_file.batch = @object
      generic_file.apply_depositor_metadata(@login_user.email)
      file = LocalFile.new(fedora_id: generic_file.id, ds_id: "content")
      options = {}
      options[:mime_type] = "audio/mp3"
      options[:file_name] = "SAMPLEA.mp3"

      uploaded = Rack::Test::UploadedFile.new(File.join(fixture_path, "SAMPLEA.mp3"), "audio/mp3")
      file.add_file uploaded, options
      file.save
      generic_file.save
      file_id = generic_file.id

      get :download, id: file_id, object_id: @object.id 
      expect(response.status).to eq(200)
      expect(response.header['Content-Type']).to eq('audio/mp3')
      expect(response.header['Content-Length']).to eq("#{File.size(File.join(fixture_path, "SAMPLEA.mp3"))}")      
    end
  end

end
